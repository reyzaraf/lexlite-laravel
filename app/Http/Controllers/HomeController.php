<?php

namespace App\Http\Controllers;
use App\Slider;
use App\Home;
use App\Founder;
use App\Partner;
use App\Page;
use App\Footer;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pages = Page::all();
        $sliders = Slider::latest()->get();
        $homes = Home::all();
        $partners = Partner::all();
        $founders = Founder::all();
        $footers = Footer::all();
        return view('home',compact('pages','sliders','homes','partners','founders','footers'));
    }
}
