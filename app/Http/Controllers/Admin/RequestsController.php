<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRequestRequest;
use App\Http\Requests\StoreRequestRequest;
use App\Http\Requests\UpdateRequestRequest;
use App\Requests;
// use App\Article;
// use App\User;

class RequestsController extends Controller
{
    //
    public function index()
    {

        $requests = Requests::all();
        return view('admin.requests.index', compact('requests'));
    }

    public function create()
    {



        return view('admin.requests.create');
    }

    public function store(StoreRequestRequest $request)
    {

        // $validated = $request->validated();
        $Request = Requests::create($request->all());

        return redirect()->route('admin.requests.index');
    }

    public function edit(Requests $Request)
    {

        return view('admin.requests.edit', compact('Request'));
    }

    public function update(UpdateRequestRequest $request, Request $Request)
    {
                $Request->update($request->all());

        return redirect()->route('admin.requests.index');
    }

    public function show(Requests $Request)
    {


        return view('admin.requests.show', compact('Request'));
    }

    public function destroy(Requests $Request)
    {


        $Request->delete();

        return back();
    }

    public function massDestroy(MassDestroyRequestRequest $request)
    {
        Requests::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
