<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyContributeRequest;
use App\Http\Requests\StoreContributeRequest;
use App\Http\Requests\UpdateContributeRequest;
use App\Contribute;

class ContributeController extends Controller
{
    public function index()
    {


        $contributes = Contribute::all();

        return view('admin.contributes.index', compact('contributes'));
    }

    public function create()
    {


        return view('admin.contributes.create');
    }

    public function store(StoreContributeRequest $request)
    {


        $contribute = Contribute::create($request->all());

        return redirect()->route('admin.contributes.index');
    }

    public function edit(Contribute $contribute)
    {
               return view('admin.contributes.edit', compact('contribute'));
    }

    public function update(StoreContributeRequest $request, Contribute $contribute)
    {
               $contribute->update($request->all());

        return redirect()->route('admin.contributes.index');
    }

    public function show(Contribute $contribute)
    {
               return view('admin.contributes.show', compact('contribute'));
    }

    public function destroy(Contribute $contribute)
    {
        // abort_unless(\Gate::allows('media_delete'), 403);

        $contribute->delete();

        return back();
    }

    public function massDestroy(MassDestroyContributeRequest $request)
    {
        Contribute::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
