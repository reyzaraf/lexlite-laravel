<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySubArticleRequest;
use App\Http\Requests\StoreSubArticleRequest;
use App\Http\Requests\UpdateSubArticleRequest;
use App\Media;
use App\SubArticle;
// use App\Article;
// use App\User;

class SubArticleController extends Controller
{
    public function index($id)
    {
        // abort_unless(\Gate::allows('media_access'), 403);
        $Subarticles = SubArticle::where('article_id',$id)->latest()->get();
        $media = Media::findOrFail($id);
        return view('admin.subarticles.index', compact('Subarticles','media'));
    }

    public function creates($id)
    {
        abort_unless(\Gate::allows('media_create'), 403);
        $Subarticles = SubArticle::where('article_id',$id)->get();
        $media = Media::findOrFail($id);

        return view('admin.subarticles.create',compact('Subarticles','media'));
    }

    public function store(StoreSubArticleRequest $request)
    {
        abort_unless(\Gate::allows('media_create'), 403);
        // $validated = $request->validated();
        $Subarticle = SubArticle::create($request->all());

        // return Redirect::oute('/anyUrl/'. $id);

        // return redirect()->back()->with(['success' => 'Data Berhasil Ditambah Silahkan Cek Artikel Kembali di Kategori Post yang anda input tadi']);
        return redirect()->back()->with(['success' => 'Data Berhasil Ditambah ']);
    }

    public function edit(SubArticle $Subarticle)
    {
        abort_unless(\Gate::allows('media_edit'), 403);

        return view('admin.subarticles.edit', compact('Subarticle'));
    }

    public function update(UpdateSubArticleRequest $request, SubArticle $Subarticle)
    {
        abort_unless(\Gate::allows('media_edit'), 403);

        $Subarticle->update($request->all());

        return redirect()->route('admin.medias.index')->with(['success' => 'data berhasil dirubah']);
    }

    public function show(SubArticle $subarticle)
    {
        abort_unless(\Gate::allows('media_show'), 403);
        // $Subarticle = SubArticle::all();
        return view('admin.subarticles.show', compact('subarticle'));
    }

    public function destroy(SubArticle $Subarticle)
    {
        abort_unless(\Gate::allows('media_delete'), 403);

        $Subarticle->delete();

        return back()->with(['warning' => 'Data Berhasil dihapus ']);
    }

    public function massDestroy(MassDestroySubArticleRequest $request)
    {
        SubArticle::whereIn('sub_id', request('ids'))->delete();

        return response(null, 204);
    }
}
