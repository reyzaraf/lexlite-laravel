<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMediaRequest;
use App\Http\Requests\StoreMediaRequest;
use App\Http\Requests\UpdateMediaRequest;
use App\Media;
class MediaController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('media_access'), 403);

        $media = Media::all();

        return view('admin.medias.index', compact('media'));
    }

    public function medialatest()
    {
        $medialatest = Media::latest()->get();
        return view('admin.media.latestmedia', compact('medialatest'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('media_create'), 403);

        return view('admin.medias.create');
    }

    public function store(StoreMediaRequest $request)
    {
        abort_unless(\Gate::allows('media_create'), 403);

        $medias = Media::create($request->all());

        return redirect()->route('admin.medias.index');
    }

    public function edit(Media $media)
    {
        abort_unless(\Gate::allows('media_edit'), 403);

        return view('admin.medias.edit', compact('media'));
    }

    public function update(UpdateMediaRequest $request, Media $media)
    {
        abort_unless(\Gate::allows('media_edit'), 403);

        $media->update($request->all());

        return redirect()->route('admin.medias.index');
    }

    public function show(Media $media)
    {
        abort_unless(\Gate::allows('media_show'), 403);

        return view('admin.medias.show', compact('media'));
    }

    public function destroy(Media $media)
    {
        abort_unless(\Gate::allows('media_delete'), 403);

        $media->delete();

        return back();
    }

    public function massDestroy(MassDestroyMediaRequest $request)
    {
        Media::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
