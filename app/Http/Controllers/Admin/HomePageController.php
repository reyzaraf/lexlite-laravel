<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyHomeRequest;
use App\Http\Requests\StoreHomeRequest;
use App\Http\Requests\UpdateHomeRequest;
use App\Home;
class HomePageController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('homepage_access'), 403);

        $homes = Home::all();

        return view('admin.homes.index', compact('homes'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('homepage_create'), 403);

        return view('admin.homes.create');
    }

    public function store(StoreHomeRequest $request)
    {
        abort_unless(\Gate::allows('homepage_create'), 403);

        $home = Home::create($request->all());

        return redirect()->route('admin.homes.index');
    }

    public function edit(Home $home)
    {
        abort_unless(\Gate::allows('homepage_edit'), 403);

        return view('admin.homes.edit', compact('home'));
    }

    public function update(UpdateHomeRequest $request, Home $home)
    {
        abort_unless(\Gate::allows('homepage_edit'), 403);

        $home->update($request->all());

        return redirect()->route('admin.homes.index');
    }

    public function show(Home $home)
    {
        abort_unless(\Gate::allows('homepage_show'), 403);

        return view('admin.homes.show', compact('home'));
    }

    public function destroy(Home $home)
    {
        abort_unless(\Gate::allows('homepage_delete'), 403);

        $home->delete();

        return back();
    }

    public function massDestroy(MassDestroyHomeRequest $request)
    {
        Home::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
