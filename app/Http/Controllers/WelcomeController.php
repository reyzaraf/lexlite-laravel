<?php

namespace App\Http\Controllers;
use App\Slider;
use App\Home;
use App\Partner;
use App\Footer;
use App\Media;
use App\SubArticle;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $pages = Page::where('category','profile')->get();

        $medias = Media::all();
        // $founders = Founder::all();
        // $footers = Footer::all();
        return view('welcome',compact('medias',));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $Subarticles = DB::table('sub_articles')
		->where('title','like',"%".$search."%")
		->paginate();
        return view('partials.search',['Subarticles' => $Subarticles]);
    }

    public function showabout()
    {

        $slug = "about";

        $homes = Home::all();
        // $partners = Partner::all();
        $footers = Footer::all();
        // $page = Page::where('slug',$slug)->first();

        // return view('partials.about',compact('homes','partners','footers'))->with('page', $page);
        return view('partials.about',compact('homes','footers'));
    }

    public function showmember()
    {

        $slug = "member";

        $homes = Home::all();
        $partners = Partner::all();
        $footers = Footer::all();
        $page = Page::where('slug',$slug)->first();

        return view('partials.about',compact('homes','partners','footers'))->with('page', $page);
    }

    public function showcontact()
    {

        $slug = "contact";

        $homes = Home::all();
        $partners = Partner::all();
        $footers = Footer::all();
        $page = Page::where('slug',$slug)->first();

        return view('partials.about',compact('homes','partners','footers'))->with('page', $page);
    }

    public function showarticle()
    {

        $medias = Media::latest()->get();
        $homes = Home::all();
        $footers = Footer::all();

        return view('partials.article',compact('medias','homes','footers'));
    }
    public function showsubarticle($id)
    {
        $Subarticles = SubArticle::where('article_id',$id)->latest()->get();
        $medias = Media::findOrFail($id);
        $homes = Home::all();
        $footers = Footer::all();

        return view('partials.article2',compact('Subarticles','medias','homes','footers'));
    }

    public function showdetail(SubArticle $subarticle)
    {
        $media = Media::all();
        // $Subarticle = SubArticle::all();
        $footers = Footer::all();
        return view('partials.detailarticle', compact('subarticle','media','footers'));
    }
    public function support()
    {
        $slug = "support";
        $homes = Home::all();
        $footers = Footer::all();

        return view('partials.support',compact('homes','footers'));
    }



}
