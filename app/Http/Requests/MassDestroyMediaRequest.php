<?php

namespace App\Http\Requests;
use Gate;
use App\Media;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('media_edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:medias,id',
        ];
    }
}
