<?php

namespace App\Http\Requests;
use App\Slider;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('slider_edit');
    }

    public function rules()
    {
        return [
            'name'     => [
                'nullable',
            ],
            'image'    => [
                'required',
            ],

        ];
    }
}
