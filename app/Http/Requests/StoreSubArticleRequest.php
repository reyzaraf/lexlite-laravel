<?php

namespace App\Http\Requests;
use App\SubArticle;
use Illuminate\Foundation\Http\FormRequest;

class StoreSubArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('media_create');
    }

    public function rules()
    {
        return [
            'title'     => [
                'required',
            ],
            'image'    => [
                'required',
            ],
            'content' => [
                'required',
            ]
        ];
    }
}
