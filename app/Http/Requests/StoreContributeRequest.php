<?php

namespace App\Http\Requests;

use App\Contribute;
use Illuminate\Foundation\Http\FormRequest;

class StoreContributeRequest extends FormRequest
{
    public function rules()
    {
        return [
            'nama' => [
                'required',
            ],
            'email' => [
                'required',
            ],
            'link' => [
                'required',
            ],
        ];
    }
}
