<?php

namespace App\Http\Requests;
use App\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function rules()
    {
        return [
            'nama'     => [
                'required',
            ],
            'email'    => [
                'required',
            ],
            'isi' => [
                'required',
            ]
        ];
    }
}
