<?php

namespace App\Http\Requests;
use App\Media;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('media_edit');
    }

    public function rules()
    {
        return [
            'title'     => [
                'required',
            ],
            'image'    => [
                'required',
            ],
        ];
    }
}
