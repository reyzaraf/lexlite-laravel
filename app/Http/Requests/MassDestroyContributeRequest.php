<?php

namespace App\Http\Requests;

use App\Contribute;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyContributeRequest extends FormRequest
{

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:contributes,id',
        ];
    }
}
