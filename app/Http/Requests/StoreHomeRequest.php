<?php

namespace App\Http\Requests;
use App\Home;

use Illuminate\Foundation\Http\FormRequest;

class StoreHomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('home_create');
    }

    public function rules()
    {
        return [
            'about'     => [
                'required',
            ],
            'video'    => [
                'required',
            ],
            'video_text' =>[
                'required'
            ]
        ];
    }
}
