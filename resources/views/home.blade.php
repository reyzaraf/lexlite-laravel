@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-body">
			<div class="row">
                <div class="col-md-6 col-lg-6">
                    <h1 class="p-3">
                        Halo {{ Auth::user()->name }}
                    </h1>
                    <h5 class="p-3">Today is {{ date('d M Y , H:i') }}</h5>
                </div>
                <div class="col-md-6 col-lg-6">
                    <h3 class="p-3">Your Email is : {{ Auth::user()->email }}</h3>
                </div>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="card shadow-sm">
				<div class="card-header acc-color">
				  <b class="white-text">Category Added</b>
				</div>
				<div class="card-body">
					<ul class="list-group list-group-flush">
						{{--  <li class="list-group-item">Cras justo odio</li>  --}}
                        @foreach ($media as $medias)
                            <li class="list-group-item">{{ $medias->title }} -  {{date('d M Y ', strtotime($medias->created_at))}}</li>
                        @endforeach
					</ul>
				</div>
			</div>
		</div>
        <div class="col-md-6">
			<div class="card shadow-sm">
				<div class="card-header acc-color">
				  <b class="white-text">New Post Added</b>
				</div>
				<div class="card-body">
					<ul class="list-group list-group-flush">
						{{--  <li class="list-group-item">Cras justo odio</li>  --}}
                        @foreach ($subarticle as $subarticles)
                            <li class="list-group-item">{{ $subarticles->title }} -  {{date('d M Y, H:i', strtotime($subarticles->created_at))}}</li>
                        @endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
@parent

@endsection
