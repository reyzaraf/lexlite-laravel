@extends('layouts.admin')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.article.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.subarticles.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <input type="hidden" name="article_id" value="{{ $media->id }}" readonly>
                <label for="name">{{ trans('global.article.fields.title') }}*</label>
                <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($article) ? $article->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
            </div>

            <d<d class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                      </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image">
                  </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">
                  <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
                  <script>
                  var route_prefix = "http://localhost:8000/laravel-filemanager";
 $('#lfm').filemanager('image', {prefix: route_prefix});
                </script>
                </d>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
                <label for="name">{{ trans('global.article.fields.content') }}*</label>
                <textarea name="content" class="form-control my-editor"></textarea>
                <script>
                    var editor_config = {
                      path_absolute : "/",
                      selector: 'textarea.my-editor',
                      relative_urls: false,
                      plugins: [
                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table directionality",
                        "emoticons template paste textpattern"
                      ],
                      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                      file_picker_callback : function(callback, value, meta) {
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
                        if (meta.filetype == 'image') {
                          cmsURL = cmsURL + "&type=Images";
                        } else {
                          cmsURL = cmsURL + "&type=Files";
                        }

                        tinyMCE.activeEditor.windowManager.openUrl({
                          url : cmsURL,
                          title : 'Filemanager',
                          width : x * 0.8,
                          height : y * 0.8,
                          resizable : "yes",
                          close_previous : "no",
                          onMessage: (api, message) => {
                            callback(message.content);
                          }
                        });
                      }
                    };

                    tinymce.init(editor_config);
                  </script>
            </div>

            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
