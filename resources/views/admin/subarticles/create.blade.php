@extends('layouts.admin')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.article.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.subarticles.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <input type="hidden" name="article_id" value="{{ $media->id }}" readonly>
                <label for="name">{{ trans('global.article.fields.title') }}*</label>
                <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($article) ? $article->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                      </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image">
                  </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">
                  <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
                  <script>
                  var route_prefix = "http://localhost:8000/laravel-filemanager";
 $('#lfm').filemanager('image', {prefix: route_prefix});
                </script>
                </div>

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.article.fields.title') }}*</label>
                    <input type="text" id="content" name="content" class="form-control" value="{{ old('title', isset($article) ? $article->content : '') }}">
                    @if($errors->has('content'))
                        <em class="invalid-feedback">
                            {{ $errors->first('content') }}
                        </em>
                    @endif
                </div>


            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
