@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.article.fields.title') }}
                    </th>
                    <td>
                        {{ $media->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.article.fields.image') }}
                    </th>
                    <td>
                        {{ $media->image }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.article.fields.content') }}
                    </th>
                    <td>
                        {!! $media->content !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

@endsection
