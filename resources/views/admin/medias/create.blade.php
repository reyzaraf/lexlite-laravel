@extends('layouts.admin')
@section('content')

<div class="card">
  <div class="card-header">
    {{ trans('global.create') }} {{ trans('global.article.title_singular') }}
  </div>

  <div class="card-body">
    <form action="{{ route("admin.medias.store") }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <label for="name">{{ trans('global.article.fields.title') }}*</label>
        <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($media) ? $media->title : '') }}">

        @if($errors->has('title'))
        <em class="invalid-feedback">
        {{ $errors->first('title') }}
        </em>
        @endif

      </div>

      <div class="form-group">
        <div class="input-group">
          <span class="input-group-btn white-text">
            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
              <i class="fa fa-picture-o"></i> Choose
            </a>
          </span>
          <input id="thumbnail" class="form-control" type="text" name="image">
        </div>
        <img id="holder" style="margin-top:15px;max-height:100px;">

      </div>

      <div class="form-group">
        <textarea class="my-editor"></textarea>
      </div>

      <div>
        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
      </div>

    </form>
  </div>
</div>
@endsection

@section('scripts')

<script src="{{ url('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script>
  var route_prefix = "{{ url('laravel-filemanager') }}";
  $('#lfm').filemanager('image', {prefix: route_prefix});
</script>


<script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
var editor_config = {
  path_absolute : "/",
  selector: "textarea.my-editor",
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern"
  ],

  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

  relative_urls: false,
  file_browser_callback : function(field_name, url, type, win) {
    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
    if (type == 'image') {
      cmsURL = cmsURL + "&type=Images";
    } else {
      cmsURL = cmsURL + "&type=Files";
    }
    tinyMCE.activeEditor.windowManager.open({
      file : cmsURL,
      title : 'Filemanager',
      width : x * 0.8,
      height : y * 0.8,
      resizable : "yes",
      close_previous : "no"
    });
  }
};
tinymce.init(editor_config);
</script>
@endsection