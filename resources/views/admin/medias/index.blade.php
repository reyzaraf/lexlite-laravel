@extends('layouts.admin')

@section('content')

@can('media_create')
<div style="margin-bottom: 10px;" class="row">
  <div class="col-lg-12">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
    @if ($message = Session::get('warning'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <a class="btn btn-success" href="{{ route("admin.medias.create") }}">
      Add  Category Post
    </a>
  </div>
</div>
@endcan

<div class="card">
  <div class="card-header">
    Post Category List
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class=" table table-bordered table-striped table-hover datatable">
        <thead>
          <tr>
            <th width="10">
            </th>
            <th>
              {{ trans('global.article.fields.title') }}
            </th>
            <th>
              {{ trans('global.article.fields.image') }}
            </th>
            {{--  <th>
              {{ trans('global.article.fields.content') }}
            </th>  --}}
            <th>
              &nbsp; Option
            </th>
          </tr>
        </thead>
        <tbody>
          @foreach($media as $key => $media)
          <tr data-entry-id="{{ $media->id }}">
            <td>
            </td>
            <td>
              {{ $media->title ?? '' }}
            </td>
            <td>
              <img height="200" src=" {{ $media->image ?? '' }}" alt="">
            </td>
            {{--  <td>
              {!! substr( $media->content,0,150 ?? '') !!}
            </td>  --}}
            <td>
              @can('media_show')
              <a class="btn btn-xs btn-primary" href="{{ route('admin.medias.show', $media->id) }}">
                Show
              </a>
              @endcan
              <a class="btn btn-xs btn-success" href="{{ route('admin.subarticles.indexs', $media->id) }}">
                View All Article
              </a>
              @can('media_edit')
              <a class="btn btn-xs btn-info" href="{{ route('admin.medias.edit', $media->id) }}">
                {{ trans('global.edit') }}
              </a>
              @endcan
              @can('media_delete')
              <form action="{{ route('admin.medias.destroy', $media->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
              </form>
              @endcan
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@parent
<script>
$(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.medias.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')
        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }
        }).done(function () { location.reload() })
      }
    }
  }

  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  @can('media_delete')
  dtButtons.push(deleteButton)
  @endcan
  
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})
</script>

@endsection
