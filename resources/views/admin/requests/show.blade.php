@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        Nama
                    </th>
                    <td>
                        {{ $Request->nama }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td>
                        {{ $Request->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Tanggal Request
                    </th>
                    <td>

                        {{ \Carbon\Carbon::parse($Request->created_at)->format('j F Y ') }}
                    </td>
                </tr>
                <tr>
                    <th>
                        isi Request dari Pengunjung
                    </th>
                    <td>
                        {!! $Request->isi !!}
                    </td>
                </tr>


            </tbody>
        </table>
    </div>
</div>

@endsection
