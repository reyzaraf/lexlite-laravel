@extends('layouts.admin')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<div class="card">
    <div class="card-header">
       Request Article
    </div>

    <div class="card-body">
        <form action="{{ route("admin.requests.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Nama*</label>
                <input type="text" id="name" name="nama" class="form-control" value="{{ old('nama', isset($request) ? $request->nama : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Email*</label>
                <input type="text" id="email" name="email" class="form-control" value="{{ old('name', isset($request) ? $request->email : '') }}">
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Konten Seperti Apa yang ingin anda Request*</label>
                <textarea name="isi" class="form-control my-editor">{{ old('isi', isset($request) ? $request->isi : '') }}</textarea>
            </div>


            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
