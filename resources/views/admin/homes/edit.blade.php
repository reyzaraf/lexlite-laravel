@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.home.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.homes.update", [$home->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('about') ? 'has-error' : '' }}">
                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                <label for="name">{{ trans('global.home.fields.about') }}*</label>
                <textarea name="about" class="form-control my-editor">
                  {!! old('about', isset($home) ? $home->about : '') !!}
                </textarea>
                <script>
                  var editor_config = {
                    path_absolute : "/",
                    selector: "textarea.my-editor",
                    plugins: [
                      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                      "searchreplace wordcount visualblocks visualchars code fullscreen",
                      "insertdatetime media nonbreaking save table contextmenu directionality",
                      "emoticons template paste textcolor colorpicker textpattern"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                    relative_urls: false,
                    file_browser_callback : function(field_name, url, type, win) {
                      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
                
                      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                      if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                      } else {
                        cmsURL = cmsURL + "&type=Files";
                      }
                
                      tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                      });
                    }
                  };
                
                  tinymce.init(editor_config);
                </script>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.home.fields.video') }}URL *</label>
                <input type="text" id="name" name="video" class="form-control" value="{{ old('video', isset($home) ? $home->video : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.home.fields.video_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('video_text') ? 'has-error' : '' }}">
                <label for="video_text">{{ trans('global.home.fields.video_text') }}*</label>
                <input type="video_text" id="video_text" name="video_text" class="form-control" value="{{ old('video_text', isset($home) ? $home->video_text : '') }}">
                @if($errors->has('video_text'))
                    <em class="invalid-feedback">
                        {{ $errors->first('video_text') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.home.fields.video_text_helper') }}
                </p>
            </div>
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection