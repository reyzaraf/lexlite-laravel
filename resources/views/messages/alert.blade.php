@foreach(['success','danger','info','warning'] as $messages)
@if (Session::has('alert-'.$messages))
<div class="alert alert-{{ $messages }} alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
    {{ Session::get('alert-'.$messages) }}
</div>
@endif
@endforeach