@extends('partials.header')
@extends('partials.nav')
<div class="jumbotron paral paralsec mt-5">
        <h1 class="display-3 text-white mt-5">{{ $page->title }}</h1>
        <p class="lead text-white mt-5">Good Corporate for Governance</p>
        </div>
    <!-- end of header -->
<div class="container">
<div class="row">
    <div class="col-sm-12">
        {!! $page->content !!}
    </div>
</div>
</div>
@extends('partials.footer')