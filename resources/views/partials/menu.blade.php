<div class="sidebar prim-color">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a href="{{ route("admin.home") }}" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt">
          </i>
          {{ trans('global.dashboard') }}
        </a>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle">
          <i class="fas fa-users nav-icon">
          </i>
          {{ trans('global.userManagement.title') }}
        </a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
              <i class="fas fa-unlock-alt nav-icon">
              </i>
              {{ trans('global.permission.title') }}
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
              <i class="fas fa-briefcase nav-icon">
              </i>
              {{ trans('global.role.title') }}
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
              <i class="fas fa-user nav-icon">
              </i>
              {{ trans('global.user.title') }}
            </a>
          </li>
        </ul>
      </li>
      {{--  <li class="nav-item">
        <a href="{{ route("admin.medias.index") }}" class="nav-link {{ request()->is('admin/medias') || request()->is('admin/medias/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.article.title') }}
        </a>
      </li>  --}}
      {{-- <li class="nav-item">
        <a href="{{ route("admin.resources.index") }}" class="nav-link {{ request()->is('admin/resources') || request()->is('admin/resources/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.article.title') }}  resource
        </a>
      </li> --}}
      <li class="nav-item">
        <a href="{{ route("admin.contributes.index") }}" class="nav-link {{ request()->is('admin/contributes') || request()->is('admin/contributes/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          Kontributor User
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route("admin.medias.index") }}" class="nav-link {{ request()->is('admin/medias') || request()->is('admin/medias/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          Media
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route("admin.requests.index") }}" class="nav-link {{ request()->is('admin/requests') || request()->is('admin/requests/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          Request Content
        </a>
      </li>
      {{-- <li class="nav-item">
        <a href="{{ route("admin.homes.index") }}" class="nav-link {{ request()->is('admin/homes') || request()->is('admin/homes/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.home.title') }}
        </a>
      </li> --}}
      {{-- <li class="nav-item">
        <a href="{{ route("admin.pages.index") }}" class="nav-link {{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.page.title') }}
        </a>
      </li> --}}
      {{-- <li class="nav-item">
        <a href="{{ route("admin.partners.index") }}" class="nav-link {{ request()->is('admin/partners') || request()->is('admin/partners/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.partner.title') }}
        </a>
      </li> --}}
      <li class="nav-item">
        <a href="{{ route("admin.sliders.index") }}" class="nav-link {{ request()->is('admin/sliders') || request()->is('admin/sliders/*') ? 'active' : '' }}">
          <i class="fas fa-cogs nav-icon">
          </i>
          {{ trans('global.slider.title') }}
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route("logout") }}" class="nav-link">
          <i class="nav-icon fas fa-sign-out-alt">
          </i>
          {{ trans('global.logout') }}
        </a>
      </li>
    </ul>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
      <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; height: 1869px; right: 0px;">
      <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
    </div>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
