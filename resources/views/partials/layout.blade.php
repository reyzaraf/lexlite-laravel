<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/material-color.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <title>LexLite</title>
  </head>
  <body>
    <form method="GET" action="/search">
      <nav id="navbar-shadow" class="navbar navbar-expand-sm navbar-dark py-md-4 sticky-top prim-color">
        <div class="container mr-0">
          <a class="navbar-brand acc-color-text" href="{{ url('/') }}"><b>LexLite</b></a>
          <button id="custom-toggler" class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item px-md-4">
                <a class="nav-link" href="{{ route('edusection.index') }}">Edu Section</a>
              </li>
              {{--  <li class="nav-item px-md-4">
                <a class="nav-link" href="#">Contribute</a>
              </li>  --}}
              {{--  <li class="nav-item px-md-4">
                <a class="nav-link" href="#">Request Content</a>
              </li>  --}}
              <li class="nav-item px-md-4">
                <a class="nav-link" href="{{ url('about') }}">About Us</a>
              </li>
              <li class="nav-item px-md-4">
                <a class="nav-link" href="{{ url('support') }}">Support Us</a>
              </li>
              <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="#">Action</a>
                          <a class="dropdown-item" href="#">Another action</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#">Something else here</a>
                    </div>
              </li> -->
            </ul>
            <div class="form-inline my-2 my-lg-0">
              <div class="d-sm-none">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="sBtn" >Search</button>
              </div>
              <div id="search-icon" class="d-none d-sm-block">
                <input type="search" name="keyword" placeholder="Search">
              </div>
            </div>
          </div>
        </div>
        <input type="submit" class="btn btn-dark red white-text mr-5" style="width: 45px; height: 45px; border-radius: 30px; font-size: 0.7em; font-weight: bold" value="GO">
      </nav>
    </form>
    
    @yield('content')

    <footer class="container py-5">
      <div class="row">
        <div class="col-12 col-md">
          LexLite
          <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
        </div>
        <div class="col-6 col-md">
          <h5>About</h5>
          <ul class="list-unstyled text-small">
            <li><a class="text-muted" href="#">Team</a></li>
            <li><a class="text-muted" href="#">Locations</a></li>
            <li><a class="text-muted" href="#">Privacy</a></li>
            <li><a class="text-muted" href="#">Terms</a></li>
          </ul>
        </div>
        <div class="col-6 col-md">
          <h5>Features</h5>
          <ul class="list-unstyled text-small">
            <li><a class="text-muted" href="#">Cool stuff</a></li>
            <li><a class="text-muted" href="#">Random feature</a></li>
            <li><a class="text-muted" href="#">Team feature</a></li>
          </ul>
        </div>
        <div class="col-6 col-md">

        </div>
      </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/6972b7bfd3.js" crossorigin="anonymous"></script>
    <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
    <script src="{{ asset('js/plyr.js' )}}"></script>
    <script>
      const player = new Plyr('#player');
    </script>
    <script>
        var input = document.getElementById("search");
            input.addEventListener("keyup", function(event) {
            if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("sBtn").click();
            }
            });
    </script>
    <script src="{{ asset('js/style.js' )}}"></script>
    <script>
      var input = document.getElementById("search");

      // Execute a function when the user releases a key on the keyboard
      input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
          // Cancel the default action, if needed
          event.preventDefault();
          // Trigger the button element with a click
          // document.getElementById("myBtn").click();
          console.log('WOI');
        }
      });
    </script>

  </body>
</html>
