@extends('partials.header')
@extends('partials.nav')
<div class="container pt-5 mt-5">
    <div class="mt-5 pt-5 mb-5 pb-5">
            <h1>{{ $news->title }}</h1>
            <span><b> {{ \Carbon\Carbon::parse($news->created_at)->format('D, d F Y') }} </b></span><br>
            <img class="img-fluid  pt-3 pb-3" style="height:350px;object-fit: cover;    " src="{{ $news->image }}" alt="">
            {!! $news->content !!}
    </div>
</div>
@extends('partials.footer')