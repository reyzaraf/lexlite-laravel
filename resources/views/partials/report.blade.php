@extends('partials.header')
@extends('partials.nav')
<div class="jumbotron paral paralsec mt-5">
        <h1 class="display-2 text-white mt-5 text-center">report
            </h1>
        </div>
    <!-- end of header -->
<div class="container">
<div class="row">
      <div class="col-sm-12">
        @foreach ($reports as $report)
            
        <!-- start card content -->
            <div class="card w-100 flex-md-row mb-4 shadow-sm h-md-250">
                <div class="card-body w-50 d-flex flex-column align-items-start">
                    <h6 class="mb-0">
                        <a class="text-dark" href="{{ $report->id }}">{{ $report->title }}</a>
                    </h6>
                    <div class="mb-1 text-muted small">{{ \Carbon\Carbon::parse($report->created_at)->format('d F, Y') }}</div>
                    <p class="card-text mb-auto">{!! substr($report->content,0,200) !!}....</p>
                    <a class="btn btn-outline-success btn-sm" href="{{ $report->id }}">Detail Report</a>
                </div>
                <img class="card-img-right flex-auto d-none d-lg-block w-50" alt="Thumbnail [200x250]" src="{{ $report->image }}" style="width: 200px; height: 250px;">
            </div>
            <!-- end card -->
            @endforeach  
        
      </div>

</div>
</div>
@extends('partials.footer')