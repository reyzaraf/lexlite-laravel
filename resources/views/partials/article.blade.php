@extends('partials.layout')

@section('content')

<div id="article" class="prim-color py-5">
  <h1 class="display-3 text-center acc-color-text mb-3">All New Category</h1>
  <div class="container">
    <div class="row">
      @foreach ($medias as $media)
      <div class="col-lg-4 col-md-6 col-sm-12 mb-5">
        <div class="card shadow">
          {{-- <img src="{{ $media->image }}" class="card-img-top" alt="Post Image"> --}}
          <div class="card-img" style="background-image: url('{{ $media->image }}')"></div>
          <div class="card-body">
            <h5 class="card-title text-center">
              <a href="#" class="prim-color-text">{{ $media->title }}</a>
            </h5>
            <a href="{{ route('subarticle.ind', $media->id) }}" class="btn acc-color-text btn-transparent btn-block">See Category</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

@endsection
