@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
	<div class="col-md-5">
		<div class="card prim-color p-4 shadow" style="border: none">
			<div class="card-body">
				@if(\Session::has('message'))
					<p class="alert alert-info">
						{{ \Session::get('message') }}
					</p>
				@endif
				<form method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}
					<h1>
						<div class="login-logo acc-color-text text-center font-weight-bold">
							{{ trans('global.site_title') }}
						</div>
					</h1>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
						</div>
						<input name="email" type="text" class="form-control @if($errors->has('email')) is-invalid @endif" placeholder="{{ trans('global.login_email') }}">
						@if($errors->has('email'))
							<em class="invalid-feedback">
								{{ $errors->first('email') }}
							</em>
						@endif
					</div>
					<div class="input-group mb-2">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-lock"></i></span>
						</div>
						<input name="password" type="password" class="form-control @if($errors->has('password')) is-invalid @endif" placeholder="{{ trans('global.login_password') }}">
						@if($errors->has('password'))
							<em class="invalid-feedback">
								{{ $errors->first('password') }}
							</em>
						@endif
					</div>
					<label>
						<input class="" name="remember" type="checkbox" /> <span class="text-white">{{ trans('global.remember_me') }}</span>
					</label>
					<div class="row">
						<div class="col-6">
							<input type="submit" class="btn btn-danger px-4" value='{{ trans('global.login') }}'>
						</div><br>
						<div class="col-12 text-right">
							<a class="btn btn-link px-0 text-white" href="{{ route('password.request') }}">
								{{ trans('global.forgot_password') }}
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
