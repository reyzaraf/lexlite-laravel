$(function () {
	$(document).scroll(function () {    	
		var y = $(window).scrollTop();
		var $nav = $("#navbar-shadow");
		if (y > ($nav.height()+24)) {
			$("#navbar-shadow").addClass('scrolled');
			$("#navbar-shadow").removeClass('on-top');
		} else {
			$("#navbar-shadow").removeClass('scrolled');
			$("#navbar-shadow").addClass('on-top');
		}
		    	
	});
});
